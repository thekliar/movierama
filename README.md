# MovieRama

## Project structure

The project structure is divided into two Maven modules:

* `movierama-server` which contains Java code for the application backend.
* `movierama-ui`: which contains the Angular framework source code for the frontend.

This tech-stack used is the following:

1. Spring Boot v2.3.4
2. Angular v10.0.3
3. H2 Embedded
4. Maven
5. Docker

## Running the application

You can run the movierama app by executing:

```bash
docker run -p 8080:8080 thekliar/workable-movierama:latest
```

This will pull and start the image found on `https://hub.docker.com/r/thekliar/workable-movierama` .

## Building the application

You can build the application as a single artifact by running:

```bash
./mvnw clean package
```

This will produce a single jar artifact, which is then packaged in a docker image.

You can either run the application by executing:

```bash
$ java -jar movierama-server/target/movierama.jar
```

Or via docker:

```bash
$ docker run -p 8080:8080 thekliar/workable-movierama:latest
```

The application will be accessible on `http://localhost:8080`.

