package com.workable.movierama.web;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.workable.movierama.dto.LoginInfoDTO;
import com.workable.movierama.dto.UserDTO;
import com.workable.movierama.service.UserService;
import com.workable.movierama.utils.JSONUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
@AutoConfigureMockMvc(addFilters = false)
class UserControllerTest {

  @MockBean
  private UserService service;

  @Autowired
  private MockMvc mockMvc;

  public static final String URL = "/api/user";

  @AfterEach
  void tearDown() {
    reset(service);
  }

  @Test
  public void registersSuccessfully() throws Exception {
    UserDTO userDTO = new UserDTO();
    userDTO.setUsername("dummy.user");
    userDTO.setPassword("dummy.password");
    doNothing().when(service).register(userDTO);

    mockMvc.perform(post(URL + "/register")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(JSONUtils.object2Json(userDTO)))
        .andExpect(status().isOk())
        .andReturn();

    verify(service, times(1)).register(userDTO);
  }

  @Test
  public void registerInvalidUserException() throws Exception {
    UserDTO invalidUserDTO = new UserDTO();
    doNothing().when(service).register(invalidUserDTO);

    mockMvc.perform(post(URL + "/register")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(JSONUtils.object2Json(invalidUserDTO)))
        .andExpect(status().is5xxServerError())
        .andReturn();

    verify(service, never()).register(invalidUserDTO);
  }

  @Test
  public void loginSuccessfully() throws Exception {
    LoginInfoDTO loginInfoDTO = new LoginInfoDTO();
    loginInfoDTO.setUsername("dummy.user");
    loginInfoDTO.setPassword("dummy.password");
    String expected = "jwt-token";
    when(service.login(loginInfoDTO)).thenReturn(expected);

    MvcResult mvcResult = mockMvc.perform(post(URL + "/login")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(JSONUtils.object2Json(loginInfoDTO)))
        .andExpect(status().isOk())
        .andReturn();

    String actual = mvcResult.getResponse().getHeader(HttpHeaders.AUTHORIZATION);

    assertEquals(expected, actual);
    verify(service, times(1)).login(loginInfoDTO);
  }

}