package com.workable.movierama.web;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.querydsl.core.types.Predicate;
import com.workable.movierama.dto.MovieDTO;
import com.workable.movierama.model.ReactionType;
import com.workable.movierama.service.MovieService;
import com.workable.movierama.utils.JSONUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MovieController.class)
@AutoConfigureMockMvc(addFilters = false)
class MovieControllerTest {

  @MockBean
  private MovieService service;

  @Autowired
  private MockMvc mockMvc;

  public static final String SECURED_URL_PREFIX = "/api/secured/movies";
  public static final String UNSECURED_URL_PREFIX = "/api/movies";

  @AfterEach
  void tearDown() {
    reset(service);
  }

  MovieDTO validMovieDTO = createNewValidMovieDTO();

  @Test
  public void savesSuccessfully() throws Exception {
    doNothing().when(service).save(validMovieDTO);

    mockMvc.perform(post(SECURED_URL_PREFIX)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(JSONUtils.object2Json(validMovieDTO)))
        .andExpect(status().isOk())
        .andReturn();

    verify(service, times(1)).save(validMovieDTO);
  }

  @Test
  public void saveInvalidBuildingException() throws Exception {
    MovieDTO invalidMovieDTO = new MovieDTO();
    doNothing().when(service).save(invalidMovieDTO);

    mockMvc.perform(post(SECURED_URL_PREFIX)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .content(JSONUtils.object2Json(invalidMovieDTO)))
        .andExpect(status().is5xxServerError())
        .andReturn();

    verify(service, never()).save(invalidMovieDTO);
  }

  @Test
  public void findsOneSuccessfully() throws Exception {
    String id = UUID.randomUUID().toString();
    when(service.findOne(id)).thenReturn(validMovieDTO);

    mockMvc.perform(get(UNSECURED_URL_PREFIX + "/{id}", id)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();
    verify(service, times(1)).findOne(id);
  }

  @Test
  public void findsAllSuccessfully() throws Exception {
    mockMvc.perform(get(UNSECURED_URL_PREFIX)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();

    verify(service, times(1)).findAll(any(Predicate.class), any(Sort.class));
  }

  @Test
  public void addsReactionSuccessfully() throws Exception {
    String movieId = UUID.randomUUID().toString();
    ReactionType userReaction = ReactionType.LIKE;
    doNothing().when(service).addUserReaction(movieId, userReaction);

    String url = SECURED_URL_PREFIX + "/" + movieId + "/reaction/" + userReaction.name();
    mockMvc.perform(put(url)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .param("id", movieId)
        .param("reaction", userReaction.name()))
        .andExpect(status().isOk())
        .andReturn();

    verify(service, times(1)).addUserReaction(movieId, userReaction);
  }

  @Test
  public void addsInvalidReactionProducesError() throws Exception {
    String movieId = UUID.randomUUID().toString();
    String userReaction = "RANDOM_REACTION";

    String url = SECURED_URL_PREFIX + "/" + movieId + "/reaction/" + userReaction;
    mockMvc.perform(put(url)
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)
        .param("id", movieId)
        .param("reaction", userReaction))
        .andExpect(status().is5xxServerError())
        .andReturn();

    verify(service, never()).addUserReaction(anyString(), any(ReactionType.class));
  }

  @Test
  public void deletesSuccessfully() throws Exception {
    String movieId = UUID.randomUUID().toString();
    mockMvc.perform(delete(SECURED_URL_PREFIX + "/{id}/reaction", movieId)
        .accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andReturn();
    verify(service, times(1)).deleteUserReaction(movieId);
  }

  private MovieDTO createNewValidMovieDTO() {
    MovieDTO dto = new MovieDTO();
    dto.setTitle("Sample title");
    dto.setDescription("Sample description");
    return dto;
  }
}