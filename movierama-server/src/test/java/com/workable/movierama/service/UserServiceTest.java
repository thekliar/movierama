package com.workable.movierama.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.querydsl.core.types.Predicate;
import com.workable.movierama.config.jwt.TokenProvider;
import com.workable.movierama.dto.LoginInfoDTO;
import com.workable.movierama.dto.UserDTO;
import com.workable.movierama.exception.MovieRamaException;
import com.workable.movierama.model.User;
import com.workable.movierama.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class UserServiceTest {

  @Autowired
  private UserService service;
  @MockBean
  private PasswordEncoder passwordEncoder;
  @MockBean
  private UserRepository repository;
  @MockBean
  private TokenProvider tokenProvider;
  @MockBean
  private AuthenticationManager authenticationManager;

  @Test
  public void registersSuccessfully() {
    String pass = "pass";
    String user = "user";
    UserDTO userDTO = new UserDTO().setUsername(user).setPassword(pass);

    when(repository.exists(any(Predicate.class))).thenReturn(false);
    when(passwordEncoder.encode(pass)).thenReturn(pass);
    when(repository.save(any(User.class))).thenReturn(new User());

    service.register(userDTO);

    verify(repository, times(1)).exists(any(Predicate.class));
    verify(passwordEncoder, times(1)).encode(pass);
    verify(repository, times(1)).save(any(User.class));
  }

  @Test
  public void loginsSuccessfully() {
    String pass = "pass";
    String user = "user";
    String token = "token";
    LoginInfoDTO loginInfoDTO = new LoginInfoDTO().setUsername(user).setPassword(pass);

    when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
        .thenReturn(SecurityContextHolder.getContext().getAuthentication());
    when(tokenProvider.createToken(user)).thenReturn(token);

    service.login(loginInfoDTO);

    verify(authenticationManager, times(1))
        .authenticate(any(UsernamePasswordAuthenticationToken.class));
    verify(tokenProvider, times(1)).createToken(user);
  }

  @Test
  public void loginBadCredentials() {
    String pass = "pass";
    String user = "user";
    LoginInfoDTO loginInfoDTO = new LoginInfoDTO().setUsername(user).setPassword(pass);

    when(authenticationManager.authenticate(any(UsernamePasswordAuthenticationToken.class)))
        .thenThrow(new BadCredentialsException("Could not auth"));

    assertThrows(MovieRamaException.class, () -> service.login(loginInfoDTO));

    verify(authenticationManager, times(1))
        .authenticate(any(UsernamePasswordAuthenticationToken.class));
    verify(tokenProvider, never()).createToken(user);
  }

  @Test
  public void registerFailsUsernameExists() {
    String pass = "pass";
    String user = "user";
    UserDTO userDTO = new UserDTO().setUsername(user).setPassword(pass);

    when(repository.exists(any(Predicate.class))).thenReturn(true);
    when(passwordEncoder.encode(pass)).thenReturn(pass);
    when(repository.save(any(User.class))).thenReturn(new User());

    assertThrows(MovieRamaException.class, () -> service.register(userDTO));

    verify(repository, times(1)).exists(any(Predicate.class));
    verify(passwordEncoder, never()).encode(pass);
    verify(repository, never()).save(any(User.class));
  }
}