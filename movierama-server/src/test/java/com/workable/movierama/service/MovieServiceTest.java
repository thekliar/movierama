package com.workable.movierama.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.workable.movierama.dto.MovieDTO;
import com.workable.movierama.exception.MovieRamaException;
import com.workable.movierama.model.Movie;
import com.workable.movierama.model.MovieUserReaction;
import com.workable.movierama.model.MovieUserReactionId;
import com.workable.movierama.model.MovieView;
import com.workable.movierama.model.QMovie;
import com.workable.movierama.model.ReactionType;
import com.workable.movierama.model.User;
import com.workable.movierama.repository.MovieRepository;
import com.workable.movierama.repository.MovieUserReactionRepository;
import com.workable.movierama.repository.MovieViewRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@SpringBootTest
@ExtendWith(SpringExtension.class)
class MovieServiceTest {

  @Autowired
  private MovieService service;
  @MockBean
  private MovieRepository movieRepository;
  @MockBean
  private MovieViewRepository movieViewRepository;
  @MockBean
  private UserService userService;
  @MockBean
  private MovieUserReactionRepository reactionRepository;

  public static final String TITLE = "title";
  public static final String DESCRIPTION = "description";
  public static final String USER = "user";

  @Test
  public void savesSuccessfully(){
    MovieDTO dto = new MovieDTO();
    dto.setTitle(TITLE);
    dto.setDescription(DESCRIPTION);
    service.save(dto);
    verify(movieRepository, times(1)).save(any(Movie.class));
  }

  @Test
  public void savesDuplicateMovieExpectedError(){
    MovieDTO dto = new MovieDTO();
    dto.setTitle(TITLE);
    dto.setDescription(DESCRIPTION);

    BooleanExpression predicate = QMovie.movie.title.eq(TITLE);
    when(movieRepository.exists(predicate)).thenReturn(true);

    assertThrows(MovieRamaException.class, () -> service.save(dto));
    verify(movieRepository, times(1)).exists(predicate);
    verify(movieRepository, never()).save(any(Movie.class));
  }

  @Test
  public void addsUserReactionSuccessfully(){
    String movieId = UUID.randomUUID().toString();
    ReactionType like = ReactionType.LIKE;
    when(movieRepository.findById(movieId)).thenReturn(Optional.of(new Movie()));
    when(userService.findCurrentUser()).thenReturn(new User());
    when(reactionRepository.save(any(MovieUserReaction.class))).thenReturn(new MovieUserReaction());

    service.addUserReaction(movieId, like);

    verify(movieRepository, times(1)).findById(movieId);
    verify(userService, times(1)).findCurrentUser();
    verify(reactionRepository, times(1)).save(any(MovieUserReaction.class));
  }

  @Test
  public void deletesUserReactionSuccessfully(){
    String movieId = UUID.randomUUID().toString();
    when(userService.findCurrentUser()).thenReturn(new User());
    doNothing().when(reactionRepository).deleteById(new MovieUserReactionId());

    service.deleteUserReaction(movieId);

    verify(userService, times(1)).findCurrentUser();
    verify(reactionRepository, times(1)).deleteById(any(MovieUserReactionId.class));
  }

  @Test
  public void findsAllSuccessfully() {
    BooleanBuilder predicate = new BooleanBuilder();
    Sort sort = Sort.unsorted();
    List<MovieView> modelViews = Collections.emptyList();
    when(movieViewRepository.findAll(predicate, sort)).thenReturn(modelViews);
    List<MovieDTO> actual = service.findAll(predicate, sort);

    verify(movieViewRepository, times(1)).findAll(predicate, sort);
    assertEquals(modelViews.size(), actual.size());
  }

  @Test
  public void findsOneForAnonymousUserSuccessfully() {

    AnonymousAuthenticationToken anonymousToken
        = new AnonymousAuthenticationToken("qwerty","Test",
        AuthorityUtils.createAuthorityList("ROLE_ANONYMOUS"));
    SecurityContextHolder.getContext().setAuthentication(anonymousToken);

    String movieId = UUID.randomUUID().toString();
    MovieView movieView = new MovieView();
    Instant createdOn = Instant.now();
    movieView.setId(movieId).setTitle(TITLE).setDescription(DESCRIPTION)
        .setCreatedBy(USER).setCreatedOn(createdOn).setLikesCount(100).setHatesCount(100);
    when(movieViewRepository.findById(movieId)).thenReturn(Optional.of(movieView));

    MovieDTO actual = service.findOne(movieId);

    assertEquals(movieId, actual.getId());
    assertEquals(TITLE, actual.getTitle());
    assertEquals(DESCRIPTION, actual.getDescription());
    assertEquals(USER, actual.getCreatedBy());
    assertEquals(createdOn, actual.getCreatedOn());
    assertEquals(100, actual.getLikesCount());
    assertEquals(100, actual.getHatesCount());

    verify(movieViewRepository, times(1)).findById(movieId);
  }

  @Test
  public void findsOneForEponymousUserSuccessfully() {

    UsernamePasswordAuthenticationToken eponymousToken
        = new UsernamePasswordAuthenticationToken("qwerty","Test",
        AuthorityUtils.createAuthorityList("ROLE_EPONYMOUS"));
    SecurityContextHolder.getContext().setAuthentication(eponymousToken);

    String movieId = UUID.randomUUID().toString();
    MovieView movieView = new MovieView();
    Instant createdOn = Instant.now();
    movieView.setId(movieId).setTitle(TITLE).setDescription(DESCRIPTION)
        .setCreatedBy(USER).setCreatedOn(createdOn).setLikesCount(100).setHatesCount(100);

    User user = new User();
    user.setId(UUID.randomUUID().toString());

    MovieUserReaction movieUserReaction = new MovieUserReaction();
    ReactionType expectedReactionType = ReactionType.LIKE;
    movieUserReaction.setReactionType(expectedReactionType);

    when(movieViewRepository.findById(movieId)).thenReturn(Optional.of(movieView));
    when(userService.findCurrentUser()).thenReturn(user);
    when(reactionRepository.findById(any(MovieUserReactionId.class))).thenReturn(Optional.of(movieUserReaction));

    MovieDTO actual = service.findOne(movieId);

    assertEquals(movieId, actual.getId());
    assertEquals(TITLE, actual.getTitle());
    assertEquals(DESCRIPTION, actual.getDescription());
    assertEquals(USER, actual.getCreatedBy());
    assertEquals(createdOn, actual.getCreatedOn());
    assertEquals(100, actual.getLikesCount());
    assertEquals(100, actual.getHatesCount());
    assertEquals(movieUserReaction.getReactionType(), actual.getUserReaction());

    verify(movieViewRepository, times(1)).findById(movieId);
    verify(userService, times(1)).findCurrentUser();
    verify(reactionRepository, times(1)).findById(any(MovieUserReactionId.class));
  }

}