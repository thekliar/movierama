package com.workable.movierama.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class JSONUtils {

  public static <T> String object2Json(final T obj) {
    try {
      return new ObjectMapper().registerModule(new JavaTimeModule()).writeValueAsString(obj);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

  public static <T> T json2Object(final String json, Class<T> clazz) {
    try {
      return new ObjectMapper().registerModule(new JavaTimeModule()).readValue(json, clazz);
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage());
    }
  }

}