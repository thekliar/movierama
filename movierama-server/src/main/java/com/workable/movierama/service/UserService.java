package com.workable.movierama.service;

import com.workable.movierama.config.jwt.TokenProvider;
import com.workable.movierama.dto.LoginInfoDTO;
import com.workable.movierama.dto.UserDTO;
import com.workable.movierama.exception.MovieRamaException;
import com.workable.movierama.model.QUser;
import com.workable.movierama.model.User;
import com.workable.movierama.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

  private final PasswordEncoder passwordEncoder;
  private final UserRepository repository;
  private final TokenProvider tokenProvider;
  private final AuthenticationManager authenticationManager;

  public void register(UserDTO userDto) {
    boolean userExists = repository.exists(QUser.user.username.eq(userDto.getUsername()));
    if (userExists) {
      throw new MovieRamaException("Username already exists");
    }

    User user = new User() ;
    user.setUsername(userDto.getUsername());
    user.setPassword(passwordEncoder.encode(userDto.getPassword()));

    repository.save(user);
  }

  public String login(LoginInfoDTO dto) {
    try {
      UsernamePasswordAuthenticationToken authenticationToken
          = new UsernamePasswordAuthenticationToken(
          dto.getUsername(), dto.getPassword());

      this.authenticationManager.authenticate(authenticationToken);
      return tokenProvider.createToken(dto.getUsername());
    } catch (AuthenticationException e) {
      log.error(e.getMessage());
      throw new MovieRamaException("Bad credentials");
    }
  }

  protected User findCurrentUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    String username = ((org.springframework.security.core.userdetails.User) authentication
        .getPrincipal()).getUsername();
    return repository.findByUsername(username)
        .orElseThrow(() -> new MovieRamaException("Could not find user"));
  }
}
