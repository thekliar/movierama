package com.workable.movierama.service;

import com.querydsl.core.types.Predicate;
import com.workable.movierama.dto.MovieDTO;
import com.workable.movierama.exception.MovieRamaException;
import com.workable.movierama.model.Movie;
import com.workable.movierama.model.MovieUserReaction;
import com.workable.movierama.model.MovieUserReactionId;
import com.workable.movierama.model.MovieView;
import com.workable.movierama.model.QMovie;
import com.workable.movierama.model.ReactionType;
import com.workable.movierama.model.User;
import com.workable.movierama.repository.MovieRepository;
import com.workable.movierama.repository.MovieUserReactionRepository;
import com.workable.movierama.repository.MovieViewRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class MovieService {

  private final MovieRepository movieRepository;
  private final MovieViewRepository movieViewRepository;
  private final UserService userService;
  private final MovieUserReactionRepository reactionRepository;

  Supplier<MovieRamaException> exceptionSupplier
      = () -> new MovieRamaException("Could not find movie");

  public void save(MovieDTO dto) {
    if (movieRepository.exists(QMovie.movie.title.eq(dto.getTitle()))) {
      throw new MovieRamaException("Movie already exists");
    }
    Movie entity = new Movie();
    entity.setTitle(dto.getTitle());
    entity.setDescription(dto.getDescription());
    movieRepository.save(entity);
  }

  public MovieDTO findOne(String id) {
    MovieView movie = findMovieViewById(id);
    return entityToDto(movie);
  }

  public List<MovieDTO> findAll(Predicate predicate, Sort sort) {
    final Iterable<MovieView> all = movieViewRepository.findAll(predicate, sort);
    return StreamSupport.stream(all.spliterator(), false)
        .map(this::entityToDto)
        .collect(Collectors.toList());
  }

  public void addUserReaction(String movieId, ReactionType reactionType) {
    Movie movie = findMovieById(movieId);
    User currentUser = userService.findCurrentUser();
    MovieUserReaction reaction = new MovieUserReaction(movie, currentUser, reactionType);
    reactionRepository.save(reaction);
  }

  public void deleteUserReaction(String movieId) {
    User currentUser = userService.findCurrentUser();
    reactionRepository.deleteById(new MovieUserReactionId(movieId, currentUser.getId()));
  }

  private MovieView findMovieViewById(String id) {
    return movieViewRepository.findById(id).orElseThrow(exceptionSupplier);
  }

  private Movie findMovieById(String id) {
    return movieRepository.findById(id).orElseThrow(exceptionSupplier);
  }

  private MovieDTO entityToDto(MovieView movieView) {
    MovieDTO dto = new MovieDTO();
    dto.setId(movieView.getId())
        .setCreatedBy(movieView.getCreatedBy())
        .setCreatedOn(movieView.getCreatedOn())
        .setTitle(movieView.getTitle())
        .setDescription(movieView.getDescription())
        .setLikesCount(movieView.getLikesCount())
        .setHatesCount(movieView.getHatesCount());

    if (!isAnonymous()) {
      User currentUser = userService.findCurrentUser();
      reactionRepository
          .findById(new MovieUserReactionId(movieView.getId(), currentUser.getId()))
          .ifPresent(movieUserReaction -> dto.setUserReaction(movieUserReaction.getReactionType()));
    }

    return dto;
  }

  private boolean isAnonymous() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return authentication instanceof AnonymousAuthenticationToken;
  }
}
