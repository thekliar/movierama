package com.workable.movierama.web;

import com.querydsl.core.types.Predicate;
import com.workable.movierama.dto.MovieDTO;
import com.workable.movierama.model.Movie;
import com.workable.movierama.model.MovieView;
import com.workable.movierama.model.ReactionType;
import com.workable.movierama.service.MovieService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class MovieController {

  private final MovieService service;

  @GetMapping("/movies")
  public List<MovieDTO> findAll(@QuerydslPredicate(root = MovieView.class) Predicate predicate,
      Sort sort) {
    return service.findAll(predicate, sort);
  }

  @GetMapping("/movies/{id}")
  public MovieDTO findOne(@PathVariable String id) {
    return service.findOne(id);
  }

  @PostMapping("/secured/movies")
  public void save(@RequestBody @Valid MovieDTO dto) {
    service.save(dto);
  }

  @PutMapping("/secured/movies/{id}/reaction/{reaction}")
  public void addReaction(@PathVariable String id, @PathVariable ReactionType reaction) {
    service.addUserReaction(id, reaction);
  }

  @DeleteMapping("/secured/movies/{id}/reaction")
  public void deleteReaction(@PathVariable String id) {
    service.deleteUserReaction(id);
  }

}
