package com.workable.movierama.web;

import com.workable.movierama.dto.LoginInfoDTO;
import com.workable.movierama.dto.UserDTO;
import com.workable.movierama.service.UserService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserController {

  private final UserService service;

  @PostMapping("/login")
  @SuppressWarnings("rawtypes")
  public ResponseEntity login(@RequestBody @Valid LoginInfoDTO dto) {
    String token = service.login(dto);
    HttpHeaders responseHeaders = new HttpHeaders();
    responseHeaders.set("Authorization", token);
    return ResponseEntity.ok().headers(responseHeaders).build();
  }

  @PostMapping("/register")
  public void register(@RequestBody @Valid UserDTO userDto) {
    service.register(userDto);
  }

}
