package com.workable.movierama.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Immutable;

import java.time.Instant;

@Getter
@Setter
@Entity
@Immutable
@Accessors(chain = true)
@Table(name = "movie_view")
public class MovieView {

  @Id
  @Column
  private String id;

  @Column(name = "title")
  private String title;

  @Column(name = "description")
  private String description;

  @Column(name = "created_by")
  private String createdBy;

  @Column(name = "created_on")
  private Instant createdOn;

  @Column(name = "likes_count")
  private long likesCount;

  @Column(name = "hates_count")
  private long hatesCount;
}
