package com.workable.movierama.model;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "movie_user_reaction")
public class MovieUserReaction {

  @EmbeddedId
  private MovieUserReactionId id;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId(value = "movieId")
  private Movie movie;

  @ManyToOne(fetch = FetchType.LAZY)
  @MapsId(value = "userId")
  private User user;

  @Enumerated(EnumType.STRING)
  @Column(name = "reaction")
  private ReactionType reactionType;

  public MovieUserReaction() {
  }

  public MovieUserReaction(Movie movie, User user, ReactionType reactionType) {
    this.movie = movie;
    this.user = user;
    this.reactionType = reactionType;
    this.id = new MovieUserReactionId(movie.getId(), user.getId());
  }
}