package com.workable.movierama.exception;

public class MovieRamaException extends RuntimeException{

  public MovieRamaException(String message) {
    super(message);
  }

  public MovieRamaException(Throwable cause) {
    super(cause);
  }

  public MovieRamaException(String message, Throwable cause) {
    super(message, cause);
  }
}
