package com.workable.movierama.config.jwt;

import static java.time.temporal.ChronoUnit.MILLIS;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Component
public class TokenProvider {

  private final String secretKey;
  private final long tokenValidityInMilliseconds;
  private final MovieRamaUserDetailsService movieRamaUserDetailsService;

  public TokenProvider(MovieRamaUserDetailsService movieRamaUserDetailsService, ObjectMapper objectMapper,
      @Value("${movierama.jwt.token-validity-in-millis}") long tokenValidityInMilliseconds,
      @Value("${movierama.jwt.secret}") String secret) {
    this.secretKey = Base64.getEncoder().encodeToString(secret.getBytes());
    this.tokenValidityInMilliseconds = tokenValidityInMilliseconds;
    this.movieRamaUserDetailsService = movieRamaUserDetailsService;
  }

  public String createToken(String username) {
    Date now = new Date();
    Date validity = Date.from(Instant.now().plus(tokenValidityInMilliseconds, MILLIS));

    final byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(this.secretKey);

    final Key signingKey = new SecretKeySpec(apiKeySecretBytes,
        SignatureAlgorithm.HS512.getJcaName());

    return Jwts.builder().setId(UUID.randomUUID().toString())
        .setSubject(username).setIssuedAt(now)
        .signWith(SignatureAlgorithm.HS512, signingKey).setExpiration(validity)
        .compact();
  }

  public Authentication getAuthentication(String token) {
    String claims = Jwts.parser().setSigningKey(this.secretKey).parseClaimsJws(token)
        .getBody().getSubject();

    UserDetails userDetails = this.movieRamaUserDetailsService.loadUserByUsername(claims);

    return new UsernamePasswordAuthenticationToken(userDetails, "",
        userDetails.getAuthorities());
  }

}
