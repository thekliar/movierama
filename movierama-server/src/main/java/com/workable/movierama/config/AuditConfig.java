package com.workable.movierama.config;

import com.workable.movierama.model.User;
import com.workable.movierama.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider", modifyOnCreate = false)
public class AuditConfig {

  @Bean
  public AuditorAware<User> auditorProvider(UserRepository userRepository) {
    return () -> {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

      if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
        return Optional.empty();
      }

      String username = ((org.springframework.security.core.userdetails.User) authentication
          .getPrincipal()).getUsername();
      return userRepository.findByUsername(username);
    };
  }
}
