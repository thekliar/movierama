package com.workable.movierama.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;

@Configuration
@EnableTransactionManagement
@Controller
public class AppConfig {

  @GetMapping("**/{path:[^.]*}")
  public String redirect() {
    return "forward:/";
  }

}
