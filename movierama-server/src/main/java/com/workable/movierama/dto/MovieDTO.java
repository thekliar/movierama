package com.workable.movierama.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.workable.movierama.model.ReactionType;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@NoArgsConstructor
@Accessors(chain = true)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MovieDTO {

  private String id;

  @JsonProperty(access = Access.READ_ONLY)
  private Instant createdOn;

  @JsonProperty(access = Access.READ_ONLY)
  private String createdBy;

  @NotEmpty
  private String title;

  @NotEmpty
  private String description;

  @JsonProperty(access = Access.READ_ONLY)
  private long likesCount;

  @JsonProperty(access = Access.READ_ONLY)
  private long hatesCount;

  @JsonProperty(access = Access.READ_ONLY)
  private ReactionType userReaction;

}
