package com.workable.movierama.repository;

import com.workable.movierama.model.MovieUserReaction;
import com.workable.movierama.model.MovieUserReactionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieUserReactionRepository extends JpaRepository<MovieUserReaction, MovieUserReactionId>,
    QuerydslPredicateExecutor<MovieUserReaction> {

}
