package com.workable.movierama.repository;

import com.workable.movierama.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, String>,
    QuerydslPredicateExecutor<Movie> {

}
