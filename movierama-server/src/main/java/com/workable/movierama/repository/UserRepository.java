package com.workable.movierama.repository;

import com.workable.movierama.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, String>,
    QuerydslPredicateExecutor<User> {

  @Transactional(propagation = Propagation.REQUIRES_NEW)
  Optional<User> findByUsername(String username);

}
