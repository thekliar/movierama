package com.workable.movierama.repository;

import com.workable.movierama.model.MovieView;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieViewRepository extends JpaRepository<MovieView, String>,
    QuerydslPredicateExecutor<MovieView> {

}
