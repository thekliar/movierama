--liquibase formatted sql
--changeset original:1

create table user
(
	id varchar(255) not null
		primary key,
	created_on datetime(6) not null,
	password varchar(255) not null,
	username varchar(255) not null unique,
	version bigint null
);

create unique index user_username_idx on user (username);

create table movie
(
	id varchar(255) not null
		primary key,
	created_on datetime(6) not null,
	description longtext not null,
	title varchar(255) not null unique,
	version bigint null,
	user_id varchar(255) not null,
	constraint FKnvsn9b9a8fok8dh383pspnxlq
		foreign key (user_id) references user (id)
);

create table movie_user_reaction
(
	reaction varchar(255) null,
	movie_id varchar(255) not null,
	user_id varchar(255) not null,
	primary key (movie_id, user_id),
	constraint FK7cj9i6ym48dynhlf8ua1dhm3r
		foreign key (user_id) references user (id),
	constraint FKf6s5xt7gdvrimda1fxi6bsm3q
		foreign key (movie_id) references movie (id)
);

create or replace view movie_view as
select movie.id,
       movie.created_on,
       us.username  created_by,
       movie.description,
       movie.title,
       (select count(*)
        from movie_user_reaction mur1
        where movie.id = mur1.movie_id
          and mur1.reaction =
              'LIKE') likes_count,
       (select count(*)
        from movie_user_reaction mur1
        where movie.id = mur1.movie_id
          and mur1.reaction =
              'HATE') hates_count
from movie join user us on movie.user_id = us.id;

