import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {Observable} from 'rxjs';
import {AppConstants} from '../app.constants';
import {LoginInfoDto} from '../dto/login-info-dto';
import {UserDto} from '../dto/user-dto';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient,
              private localHttp: HttpClient,
              private jwtService: JwtHelperService) {
  }

  static getJwt(): string {
    return sessionStorage.getItem(AppConstants.JWT_STORAGE_NAME);
  }

  isTokenExpired(){
    return this.jwtService.isTokenExpired(sessionStorage.getItem(AppConstants.JWT_STORAGE_NAME))
  }

  signUp(userDto: UserDto) {
    return this.http.post(`${AppConstants.API_ROOT}/user/register`, userDto);
  }

  login(loginInfoDTO: LoginInfoDto) {
    return this.localHttp.post(AppConstants.API_ROOT + '/user/login', loginInfoDTO, {
      headers: {
        'Content-Type': 'application/json'
      },
      observe: 'response'
    });
  }

}
