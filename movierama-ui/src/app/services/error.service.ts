import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  static getClientMessage(error: Error | any): string {
    if (!navigator.onLine) {
      return 'No Internet Connection';
    }
    return error.message ? error.message : error.reason ? error.reason : error.toString();
  }

  static getClientStack(error: Error): string {
    return error.stack;
  }

  static getServerMessage(error: HttpErrorResponse): string {
    if (typeof error.error === 'string') {
      return error.error;
    } else {
      return 'Something went wrong. Try again later.';
    }
  }

  static getServerStatus(error: HttpErrorResponse): number {
    return error.status;
  }
}
