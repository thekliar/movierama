import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConstants} from '../app.constants';
import {MovieDto} from '../dto/movie-dto';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(private http: HttpClient) {
  }

  get(movieId: string): Observable<MovieDto> {
    return this.http.get<MovieDto>(`${AppConstants.API_ROOT}/movies/${movieId}`);
  }

  save(movieDto: MovieDto) {
    return this.http.post(`${AppConstants.API_SECURED_ROOT}/movies`, movieDto);
  }

  addReaction(movieId: string, reaction: string) {
    return this.http.put(`${AppConstants.API_SECURED_ROOT}/movies/${movieId}/reaction/${reaction}`, {});
  }

  deleteReaction(movieId: string) {
    return this.http.delete(`${AppConstants.API_SECURED_ROOT}/movies/${movieId}/reaction`, {});
  }

  getAll(queryString?: string): Observable<MovieDto[]> {
    let url = `${AppConstants.API_ROOT}/movies`;

    if (queryString) {
      url = `${url}?${queryString}`;
    }
    return this.http.get<MovieDto[]>(url);
  }

}
