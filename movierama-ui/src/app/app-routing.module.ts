import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './auth/login.component';
import {RegistrationComponent} from './auth/registration.component';
import {HomeComponent} from './home/home.component';
import {AddMovieComponent} from './movie/add-movie.component';
import {CanActivateGuard} from './shared/guards/can-activate-guard';


const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'home', component: HomeComponent},
  {path: 'add-movie', component: AddMovieComponent, canActivate: [CanActivateGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
