import {AppConstants} from '../app.constants';
import {UserService} from '../services/user.service';
import {JwtHelperService} from '@auth0/angular-jwt';

export class BaseComponent {

  jwtService: JwtHelperService = new JwtHelperService();

  constructor() {
  }

  constants = AppConstants;

  isLoggedIn(): boolean {
    const jwt = UserService.getJwt();
    return jwt != null &&
      jwt !== undefined;
  }

  private getJWTDecoded(): any {
    let decoded: string;
    if (UserService.getJwt()) {
      decoded = this.jwtService.decodeToken(UserService.getJwt());
    }
    return decoded;
  }

  getUsername(): string {
    return this.getJWTDecoded()['sub'];
  }
}
