import {Component} from '@angular/core';
import {BaseComponent} from '../base-component';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent extends BaseComponent {
  title = 'movierama';

  constructor(private router: Router) {
    super();
  }

  signOut() {
    sessionStorage.clear();
    this.goToHome();
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  showUserMovies() {
    this.router.navigate(['/home'], {queryParams: {createdBy: this.getUsername()}});
  }
}
