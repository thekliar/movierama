import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Log} from 'ng2-logger/browser';
import {BaseComponent} from '../base-component';

@Injectable()
export class CanActivateGuard extends BaseComponent implements CanActivate {
  private log = Log.create('CanActivateGuard');

  constructor(private router: Router) {
    super();
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const jwtString = sessionStorage.getItem(this.constants.JWT_STORAGE_NAME);
    const redirect = window.location.href;
    const isTokenExpired = this.jwtService.isTokenExpired(jwtString);

    if (jwtString && !isTokenExpired) {
      return true;
    } else if (!jwtString) {
      this.log.info(
        `Did not find a JWT. Proceeding to login with a redirect back to ${redirect}.`);
    } else if (isTokenExpired) {
      this.log.info(
        `Found expired JWT. Removing it and proceeding to login with a redirect back to ${redirect}.`);
      sessionStorage.removeItem(this.constants.JWT_STORAGE_NAME);
    }

    this.router.navigate(['login'], {
      queryParams: {
        returnUrl: state.url
      }
    });
  }

}
