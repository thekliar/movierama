import {ErrorHandler, Injectable, Injector, NgZone} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';

import {Log} from 'ng2-logger/browser';
import {AppConstants} from '../app.constants';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material/dialog';
import {UtilityService} from '../services/utility.service';
import {UserService} from '../services/user.service';
import {ErrorService} from '../services/error.service';

@Injectable()
export class GlobalErrorHandler implements ErrorHandler {

  private logger = Log.create('GlobalErrorHandler');

  constants = AppConstants;

  constructor(private injector: Injector, private dialog: MatDialog) {
  }

  handleError(error: Error | HttpErrorResponse) {
    const userService = this.injector.get(UserService);
    const notifier = this.injector.get(UtilityService);
    const router = this.injector.get(Router);
    const ngZone = this.injector.get(NgZone);

    if (error instanceof HttpErrorResponse) {
      // Server Error
      let status = ErrorService.getServerStatus(error);
      if (status === 403 || status === 401) {
        if (userService.isTokenExpired() || error.headers.get("Reason")) {
          this.handleUnauthorizedAccess(error, userService, notifier, router, ngZone);
        } else {
          this.handleGenericServerError(error, notifier, ngZone);
        }
      } else {
        this.handleGenericServerError(error, notifier, ngZone);
      }
    } else {
      // Client Error
      this.handleClientError(error, notifier, ngZone);
    }
  }

  private handleUnauthorizedAccess(error, userService, notifier, router, ngZone) {
    if (userService.isTokenExpired()) {
      let message = 'Your token has expired. Please log-in again.';
      this.handleUnauthorizedError(message, notifier, router, ngZone);
    }
  }

  private handleClientError(error: Error | HttpErrorResponse, notifier, zone) {
    zone.run(() => {
      let message = ErrorService.getClientMessage(error);
      let stack = ErrorService.getClientStack(error);
      notifier.popupError(message);
      this.logger.error(message, stack);
    });
  }

  private handleGenericServerError(error: HttpErrorResponse, notifier, zone) {
    zone.run(() => {
      let message = ErrorService.getServerMessage(error);
        notifier.popupError(message);
        this.logger.error(message);
    });
  }

  private handleUnauthorizedError(message, notifier, router, zone) {
    zone.run(() => {
      this.logger.info(message);
      notifier.popupError(message);
      this.logger.error(message);
      this.dialog.closeAll();
      sessionStorage.removeItem(this.constants.JWT_STORAGE_NAME);
      router.navigate(['login']);
    });
  }
}
