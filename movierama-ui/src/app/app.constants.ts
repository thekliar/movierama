export const AppConstants = {
  JWT_STORAGE_NAME: 'movierama_jwt',
  API_ROOT: '/api',
  API_SECURED_ROOT: 'api/secured',

  HEADERS: {
    AUTHORIZATION: 'Authorization'
  },

  jwt: {
    claims: {
      USERNAME: 'sub'
    }
  },

  REACTION: {
    LIKE: 'LIKE',
    HATE: 'HATE',
  }
};

