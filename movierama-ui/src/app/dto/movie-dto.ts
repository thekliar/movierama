export interface MovieDto {
  id: string;
  title: string;
  description: string;
  createdBy: string;
  createdOn: Date;
  likesCount: number;
  hatesCount: number;
  userReaction: string;
}
