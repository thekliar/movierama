export interface UserDto {
  username: string;
  password: string;
  createdOn: Date;
  createdBy: string;
}
