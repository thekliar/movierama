import {Component, Input, OnInit} from '@angular/core';
import {Log} from 'ng2-logger/browser';
import {BaseComponent} from '../shared/base-component';
import {MovieDto} from '../dto/movie-dto';
import {MovieService} from '../services/movie.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent extends BaseComponent implements OnInit {

  private log = Log.create('MovieComponent');

  @Input() movie: MovieDto;

  constructor(private movieService: MovieService, private router: Router) {
    super();
  }

  ngOnInit() {
  }

  hasVotes(movie: MovieDto) {
    return this.hasLikes(this.movie) || this.hasHates(movie);
  }

  hasLikes(movie: MovieDto) {
    return movie.likesCount > 0;
  }

  hasHates(movie: MovieDto) {
    return movie.hatesCount > 0;
  }

  isPublisher(createdBy: string) {
    return createdBy === this.getUsername();
  }

  fetchMovies(createdBy: string) {
    this.router.navigate(['/home'], {queryParams: {createdBy: createdBy}});
  }

  userHasVoted(movie: MovieDto) {
    return movie.userReaction != undefined
  }

  userHasReactedWith(movie: MovieDto, reaction: string) {
    return movie.userReaction === reaction;
  }

  addReaction(movieId: string, reaction: string) {
    this.movieService.addReaction(movieId, reaction).subscribe(res => {
      this.refreshMovie(movieId);
    });
  }

  removeReaction(movieId: string) {
    this.movieService.deleteReaction(movieId).subscribe(res => {
      this.refreshMovie(movieId);
    });
  }

  private refreshMovie(movieId: string): void {
    this.movieService.get(movieId).subscribe(res => {
      this.movie = res;
    });
  }
}
