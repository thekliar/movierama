import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Log} from 'ng2-logger/browser';
import {UtilityService} from '../services/utility.service';
import {BaseComponent} from '../shared/base-component';
import {MovieDto} from '../dto/movie-dto';
import {MovieService} from '../services/movie.service';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss'],
})
export class AddMovieComponent extends BaseComponent implements OnInit {

  private log = Log.create('AddMovieComponent');
  addMovieForm: FormGroup;
  errorMessage: string;

  constructor(private router: Router,
              private fb: FormBuilder,
              private movieService: MovieService,
              private utilityService: UtilityService) {
    super();
  }

  ngOnInit() {
    this.addMovieForm = this.fb.group({
      title: ['', [Validators.required]],
      description: ['', [Validators.required]]
    });
  }

  onSubmit({value}: { value: MovieDto }) {
    this.movieService.save(value).subscribe(() => {
      let message = `${value.title} was successfully added.`;
      this.utilityService.popupSuccess(message);
      this.log.info(message);
      this.router.navigate(['/home']);
    })
  }

}
