import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Log} from 'ng2-logger/browser';
import {BaseComponent} from '../shared/base-component';
import {ActivatedRoute, Router} from '@angular/router';
import {MovieService} from '../services/movie.service';
import {MovieDto} from '../dto/movie-dto';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent extends BaseComponent implements OnInit, AfterViewInit {

  private log = Log.create('HomeComponent');

  movies: MovieDto[] = [];
  createdBy: string;

  constructor(private router: Router,
              private movieService: MovieService,
              private route: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(queryParams => {
      this.createdBy = this.route.snapshot.queryParams.createdBy;
      this.sortByDateDesc()
    });
  }

  ngAfterViewInit() {
    this.sortByDateDesc()
  }

  addMovie() {
    this.router.navigate(['/add-movie']);
  }

  sortByDateDesc() {
    this.fetchAll("sort=createdOn,desc");
  }

  sortByHatesDesc() {
    this.fetchAll("sort=hatesCount,desc");
  }

  sortByLikesDesc() {
    this.fetchAll("sort=likesCount,desc");
  }

  fetchAll(sorting: string) {
    let queryString;
    if (this.createdBy) {
      queryString = `createdBy=${this.createdBy}&${sorting}`
    } else {
      queryString = sorting;
    }
    this.movieService.getAll(queryString).subscribe(movies => {
      this.movies = movies;
    });
  }
}
