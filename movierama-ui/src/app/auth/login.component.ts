import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Log} from 'ng2-logger/browser';
import {LoginInfoDto} from '../dto/login-info-dto';
import {UtilityService} from '../services/utility.service';
import {BaseComponent} from '../shared/base-component';
import {UserService} from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent extends BaseComponent implements OnInit {

  private log = Log.create('LoginComponent');
  loginForm: FormGroup;
  errorMessage: string;

  constructor(private router: Router,
              private fb: FormBuilder,
              private utilityService: UtilityService,
              private userService: UserService) {
    super();
  }

  ngOnInit() {
    if (this.isLoggedIn()) {
      this.log.info('User is already logged in.');
      this.router.navigate(['/home']);
    }

    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  onSubmit({value}: { value: LoginInfoDto }) {
    this.userService.login(value).subscribe(onNext => {
      const token = onNext.headers.get(this.constants.HEADERS.AUTHORIZATION);
      if (token) {
        sessionStorage.setItem(this.constants.JWT_STORAGE_NAME, token);
        this.log.info("User successfully logged in");
        this.router.navigate(['/home'])
      }
    });
  }

}
