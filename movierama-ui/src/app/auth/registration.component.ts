import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Log} from 'ng2-logger/browser';
import {UtilityService} from '../services/utility.service';
import {BaseComponent} from '../shared/base-component';
import {UserService} from '../services/user.service';
import {UserDto} from '../dto/user-dto';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
})
export class RegistrationComponent extends BaseComponent implements OnInit {

  private log = Log.create('RegistrationComponent');
  signUpForm: FormGroup;
  errorMessage: string;

  constructor(private router: Router,
              private fb: FormBuilder,
              private userService: UserService,
              private utilityService: UtilityService) {
    super();
  }

  ngOnInit() {
    if (this.isLoggedIn()) {
      this.log.data('User is already logged in.');
      this.router.navigate(['/home']);
    }

    this.signUpForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      repassword: ['', [Validators.required]]
    }, {validator: RegistrationComponent.matchPassword});
  }

  onSubmit({value}: { value: UserDto }) {
    this.userService.signUp(value).subscribe(onEvent => {
      let message = `Sign up was successful. You can now log-in`;
      this.utilityService.popupSuccess(message);
      this.log.info(message);
      this.router.navigate(['/home']);
    });
  }

  private static matchPassword(control: AbstractControl) {
    const password = control.get('password').value;
    const confirmPassword = control.get('repassword').value;
    return password === confirmPassword ? null : {notSame: true}
  }
}
